﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BasicProjection
{
    public class Program
    {
        // メインメソッド
        private static void Main()
        {
            Run();
            Console.Read();
        }

        // ラン
        private static void Run()
        {
            // 入力
            Assumption asm = Assumption.Load("Assumption.csv");
            List<Cell> cells = Cell.LoadCells("Cells.csv");

            // 出力設定
            string path_resultreport = @"result\resultreport.csv";
            string path_dumpreport = @"result\dumpreport.csv";
            Directory.CreateDirectory(Path.GetDirectoryName(path_resultreport));
            Directory.CreateDirectory(Path.GetDirectoryName(path_dumpreport));

            StreamWriter sw_resultreport = new StreamWriter(path_resultreport);
            StreamWriter sw_dumpreport = new StreamWriter(path_dumpreport);

            // セルごとの計算
            for (int i = 0; i < cells.Count; i++)
            {
                // モデルの読み込み
                BaseModel model = CreateModel(asm, cells[i]);

                // ResultReportの出力
                if (i == 0) sw_resultreport.WriteLine("{0},{1}", "cell", model.ResultReportHeader());
                sw_resultreport.WriteLine("{0},{1}", i, model.ResultReportValues());

                // DumpReportの出力
                foreach (string record in model.DumpReport())
                {
                    sw_dumpreport.WriteLine("{0},{1}", i, record);
                }

            }

            // 終了処理
            sw_resultreport.Close();
            sw_dumpreport.Close();

            Console.WriteLine("finished");

        }

        // 読み込むモデルを指定するメソッド
        private static BaseModel CreateModel(Assumption asm, Cell cell)
        {
            return new WLModel(asm, cell);
        }
        
    }
}