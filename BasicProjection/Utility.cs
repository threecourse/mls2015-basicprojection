﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BasicProjection
{
    // プロジェクションの設定
    public static class Setting
    {
        static public int MaxT = 100; // 最大のプロジェクション期間
        static public int MaxX = 130; // 最大の最終年齢
    }

    // 一般的なユーティリティ変数・メソッドのクラス
    public static class GeneralUtility
    {
        public static void Foreach<T>(this IEnumerable<T> list, Action<T> action)
        {
            foreach (T e in list) { action(e); }
        }

        public static string Join<T>(this IEnumerable<T> list, string sep)
        {
            return string.Join(sep, list);
        }
    }

    // プロジェクション計算に使用するユーティリティ変数・メソッドのクラス
    public static class ProjectionUtility
    {
        /// <summary>
        /// 配列のｔ番目の値を返す（idxはt-1)
        /// </summary>
        /// <remarks>
        /// t = 0 の場合はデフォルト値を返す
        /// 配列の大きさを超えた場合、配列の最後の値を返す
        /// tベース変数はtを渡せば良く、xベース変数はx + tを渡せば良い。(x=0, t=1のときidx=0となる）
        /// </remarks>
        public static T At<T>(this T[] ary, int _t)
        {
            T d = default(T);
            //default
            if (_t == 0)
                return d;
            if (_t - 1 >= ary.Count())
                return ary.Last();
            return ary[_t - 1];
        }

        // 出力用レコードの追加（intを引数にとる関数）
        // デフォルトでは0からProjectionSetting.ProjectionPeriodまでの値を出力する
        public static void AddRecord<T>(this List<string> list, string name, Func<int, T> source, string format = ", 12:f6", int end = -1)
        {
            list.Add(Record(name, source, format, end));
        }

        // 出力用レコードの追加（配列）
        // デフォルトでは0からProjectionSetting.ProjectionPeriodまでの値を出力する
        public static void AddRecord<T>(this List<string> list, string name, T[] source, string format = ", 12:f6", int end = -1)
        {
            list.Add(Record(name, (int i) => source[i], format, end));
        }

        // 出力用レコードの追加（単一の数値）
        public static void AddRecordSingle<T>(this List<string> list, string name, T value, string format = ", 12:f6")
        {
            list.Add(RecordSingle(name, value, format));
        }

        // レコード文字列の作成（intを引数にとる関数・配列）
        private static string Record<T>(string name, Func<int, T> source, string format = ", 12:f6",  int end = -1)
        {
            if (end == -1) { end = Setting.MaxT; }
            string format0 = "{0" + format + "}";
            return name + "," + Enumerable.Range(0, end + 1).Select((int i) => string.Format(format0 , source(i))).Join(",");
        }

        // レコード文字列の作成（単一の数値）
        private static string RecordSingle<T>(string name, T value, string format = ", 12:f6")
        {
            string format0 = "{0" + format + "}";
            return name + "," + string.Format(format0 , value);
        }

    }

}



