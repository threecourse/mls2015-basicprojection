﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasicProjection
{
    // プロジェクションモデルの基本クラス
    abstract class BaseModel
    {
        // インプット
        public Assumption asm;
        public Cell cell;

        // セル変数
        public string PLAN;
        public int x;
        public int n;
        public int m;
        public string sex;
        public double S;

        // アサンプション変数
        public double[] InvestmentYield;
        public double[] qx_input;
        public double[] qwx_input;
        public double[] SelectionFactor_input;
        public double Expense_AcqS;
        public double Expense_AcqP;
        public double Expense_MaintpayingS;
        public double Expense_MaintpaidupS;
        public double Expense_MaintP;
        
        // レート
        public BaseRate Rate;

        // 生命表
        public double[] qx;
        public double[] qwx;
        public double[] lxBOY;
        public double[] lxMOY;
        public double[] lxPay;
        public double[] lxEOY;
        public double[] lxEOYaftMat;
        public double[] d_x;
        public double[] dwx;

        // CF・ストック項目
        public double[] PremiumIncome;
        public double[] DeathBenefit;
        public double[] SurrenderBenefit;
        public double[] PartialEndowBenefit;
        public double[] EndowBenefit;
        public double[] InitExpense;
        public double[] MaintExpense;
        public double[] Commission;
        public double[] InvestmentIncome;
        public double[] ReserveIncrease;
        public double[] ProfitBeforeTax;
        public double[] Reserve;
        public double[] CashValueEndT;
        public double[] AzilVEndT;
        public double[] InvestmentAsset;

        // PV項目
        public double[] PVProfitBeforeTax;
        public double[] PVPremium;
        public double[] DiscountFactor;

        // コンストラクタ
        public BaseModel(Assumption asm, Cell cell)
        {
            this.asm = asm;
            this.cell = cell;

            Initialize();
            SetCell();
            SetAssumption();
            SetRate();
            Calculate();
        }

        // 配列等の初期化
        protected virtual void Initialize()
        {
            // 生命表
            qx = new double[Setting.MaxT + 1];
            qwx = new double[Setting.MaxT + 1];
            lxBOY = new double[Setting.MaxT + 1];
            lxMOY = new double[Setting.MaxT + 1];
            lxPay = new double[Setting.MaxT + 1];
            lxEOY = new double[Setting.MaxT + 1];
            lxEOYaftMat = new double[Setting.MaxT + 1];
            d_x = new double[Setting.MaxT + 1];
            dwx = new double[Setting.MaxT + 1];

            // CF・ストック項目
            PremiumIncome = new double[Setting.MaxT + 1];
            DeathBenefit = new double[Setting.MaxT + 1];
            SurrenderBenefit = new double[Setting.MaxT + 1];
            PartialEndowBenefit = new double[Setting.MaxT + 1];
            EndowBenefit = new double[Setting.MaxT + 1];
            InitExpense = new double[Setting.MaxT + 1];
            MaintExpense = new double[Setting.MaxT + 1];
            Commission = new double[Setting.MaxT + 1];
            InvestmentIncome = new double[Setting.MaxT + 1];
            ReserveIncrease = new double[Setting.MaxT + 1];
            ProfitBeforeTax = new double[Setting.MaxT + 1];
            Reserve = new double[Setting.MaxT + 1];
            CashValueEndT = new double[Setting.MaxT + 1];
            AzilVEndT = new double[Setting.MaxT + 1];
            InvestmentAsset = new double[Setting.MaxT + 1];

            // PV項目
            PVProfitBeforeTax = new double[Setting.MaxT + 1];
            PVPremium = new double[Setting.MaxT + 1];
            DiscountFactor = new double[Setting.MaxT + 1];
        }

        // レートのセット
        protected virtual void SetRate()
        {
            Rate = Set_Rate();
        }

        // セル変数のセット
        protected virtual void SetCell()
        {
            PLAN = cell.get_str("PLAN");
            x = cell.get_int("x");
            n = cell.get_int("n");
            m = cell.get_int("m");
            sex = cell.get_str("sex");
            S = cell.get_dbl("S");
        }

        // アサンプション変数のセット
        protected virtual void SetAssumption()
        {
            InvestmentYield = asm.get_dblary("InvYield");
            qx_input = Set_qx_input();
            qwx_input = Set_qwx_input();
            SelectionFactor_input = Set_SelectionFactor_input();

            Expense_AcqS = Set_Expense_AcqS();
            Expense_AcqP = Set_Expense_AcqP();
            Expense_MaintpayingS = Set_Expense_MaintpayingS();
            Expense_MaintpaidupS = Set_Expense_MaintpaidupS();
            Expense_MaintP = Set_Expense_MaintP();
        }

        // 計算
        protected virtual void Calculate()
        {

            // 生命表
            for (int t = 0; t <= Setting.MaxT; t++)
            {
                qx[t] = (t == 0 || t > n) ? 0.0 : (qx_input.At(x + t) * SelectionFactor_input.At(t));
                qwx[t] = (t == 0 || t > n) ? 0.0 : qwx_input.At(t);

                if      (t == 0) { lxBOY[t] = 0.0; }
                else if (t == 1) { lxBOY[t] = 1.0; }
                else             { lxBOY[t] = lxEOYaftMat[t - 1]; }

                d_x[t] = (t == 0) ? 0.0 : (lxBOY[t] * qx[t] * (1.0 - qwx[t] / 2.0));
                dwx[t] = (t == 0) ? 0.0 : (lxBOY[t] * qwx[t] * (1.0 - qx[t] / 2.0));

                lxEOY[t] = Math.Max(lxBOY[t] - d_x[t] - dwx[t], 0.0);
                lxEOYaftMat[t] = (t >= n) ? 0.0 : lxEOY[t];
                lxPay[t] = (t <= m) ? lxBOY[t] : 0.0;
                lxMOY[t] = (lxBOY[t] + lxEOY[t]) / 2.0;
          
                // CF項目
                PremiumIncome[t] = (t == 0 || t > m) ? 0.0 : lxPay[t] * Rate.GrossP_Rate * S;
                DeathBenefit[t] = d_x[t] * S * DeathBen(t); ;
                SurrenderBenefit[t] = dwx[t] * S * SurrenderBen(t); ;
                PartialEndowBenefit[t] = lxBOY[t] * S * PartialEndowBen(t); ;
                EndowBenefit[t] = lxEOY[t] * S * EndowBen(t);

                if (t == 0 || t > n)
                {
                    InitExpense[t] = 0.0;
                    MaintExpense[t] = 0.0;
                }
                else
                {
                    double expense_acqS = (t > 1) ? 0.0 : Expense_AcqS;
                    InitExpense[t] = lxBOY[t] * S * expense_acqS + PremiumIncome[t] * Expense_AcqP;
                    double expense_maintS = (t > m) ? Expense_MaintpaidupS : Expense_MaintpayingS;
                    MaintExpense[t] = lxMOY[t] * S * expense_maintS + PremiumIncome[t] * Expense_MaintP;
                }

                Commission[t] = PremiumIncome[t] * CommRatio(t);

                Reserve[t] = lxEOYaftMat[t] * Rate.NetV_Rate[1][t] * S; ;
                CashValueEndT[t] = lxEOYaftMat[t] * Rate.CV_Rate[t] * S;
                AzilVEndT[t] = lxEOYaftMat[t] * Rate.AllZillV_Rate[t] * S;

                ReserveIncrease[t] = (t == 0) ? 0.0 : (Reserve[t] - Reserve[t - 1]);

                InvestmentAsset[t] = (t == 0) ? 0.0
                    : (Reserve[t - 1]
                     + PremiumIncome[t]
                     - (InitExpense[t] + Commission[t])
                     - (DeathBenefit[t] + SurrenderBenefit[t] + PartialEndowBenefit[t]
                     + EndowBenefit[t] + MaintExpense[t]) / 2.0
                      );
                InvestmentIncome[t] = InvestmentAsset[t] * InvestmentYield.At(t);

                ProfitBeforeTax[t] = (PremiumIncome[t]
                                    + InvestmentIncome[t]
                                    - (DeathBenefit[t] + SurrenderBenefit[t] + PartialEndowBenefit[t]
                                    + EndowBenefit[t] + InitExpense[t] + MaintExpense[t] + Commission[t])
                                    - ReserveIncrease[t]
                                    );
            }

            for (int t = 0; t <= Setting.MaxT; t++)
            {
                // 割引率
                DiscountFactor[t] = (t == 0) ? 1.0 : DiscountFactor[t - 1] / (1 + InvestmentYield.At(t));
            }

            for (int t = Setting.MaxT; t >= 0; t--)
            {
                // 現在価値
                PVPremium[t] = (t == Setting.MaxT) ? PremiumIncome[t] 
                                          : (PremiumIncome[t] + PVPremium[t + 1] * DiscountFactor[t + 1] / DiscountFactor[t]);
                PVProfitBeforeTax[t] = (t == Setting.MaxT) ? ProfitBeforeTax[t] 
                                                  : (ProfitBeforeTax[t] + PVProfitBeforeTax[t + 1] * DiscountFactor[t + 1] / DiscountFactor[t]);
            }
        }

        // 関数定義

        // レートのセット
        protected abstract BaseRate Set_Rate();

        // 死亡率・解約率・選択効果などの前提のセット
        protected abstract double[] Set_qx_input();
        protected abstract double[] Set_qwx_input();
        protected abstract double[] Set_SelectionFactor_input();

        // 事業費・コミッション・運用利回りの前提のセット
        protected abstract double Set_Expense_AcqS();
        protected abstract double Set_Expense_AcqP();
        protected abstract double Set_Expense_MaintpayingS();
        protected abstract double Set_Expense_MaintpaidupS();
        protected abstract double Set_Expense_MaintP();

        //  保険金額・コミッション率
        public abstract double DeathBen(int t);
        public abstract double SurrenderBen(int t);
        public abstract double PartialEndowBen(int t);
        public abstract double EndowBen(int t);
        public abstract double CommRatio(int t);

        // プロフィットマージン
        public virtual double ProfitMargin()
        {
            return PVProfitBeforeTax[0] / PVPremium[0];
        }

        // レポート
        public virtual List<string> DumpReport()
        {
            List<string> report = new List<string>();

            report.AddRange(Rate.DumpReport());
            report.AddRecordSingle("PLAN", PLAN, "");
            report.AddRecordSingle("x", x, ":f0");
            report.AddRecordSingle("n", n, ":f0");
            report.AddRecordSingle("m", m, ":f0");
            report.AddRecordSingle("sex", sex, "");
            report.AddRecordSingle("S", S, ":f6");
            report.AddRecord("t", t => t, ", 12:f0");
            report.AddRecord("lxBOY", lxBOY);
            report.AddRecord("lxMOY", lxMOY);
            report.AddRecord("lxPay", lxPay);
            report.AddRecord("lxEOY", lxEOY);
            report.AddRecord("lxEOYaftMat", lxEOYaftMat);
            report.AddRecord("d_x", d_x);
            report.AddRecord("dwx", dwx);
            report.AddRecord("PremiumIncome", PremiumIncome, ", 12:f0");
            report.AddRecord("DeathBenefit", DeathBenefit, ", 12:f0");
            report.AddRecord("SurrenderBenefit", SurrenderBenefit, ", 12:f0");
            report.AddRecord("PartialEndowBenefit", PartialEndowBenefit, ", 12:f0");
            report.AddRecord("EndowBenefit", EndowBenefit, ", 12:f0");
            report.AddRecord("InitExpense", InitExpense, ", 12:f0");
            report.AddRecord("MaintExpense", MaintExpense, ", 12:f0");
            report.AddRecord("Commission", Commission, ", 12:f0");
            report.AddRecord("InvestmentIncome", InvestmentIncome, ", 12:f0");
            report.AddRecord("ReserveIncrease", ReserveIncrease, ", 12:f0");
            report.AddRecord("ProfitBeforeTax", ProfitBeforeTax, ", 12:f0");
            report.AddRecord("Reserve", Reserve, ", 12:f0");
            report.AddRecord("PVProfitBeforeTax", PVProfitBeforeTax, ", 12:f0");
            report.AddRecord("PVPremium", PVPremium, ", 12:f0");
            report.AddRecord("DiscountFactor", DiscountFactor);
            report.AddRecordSingle("ProfitMargin", ProfitMargin());
            return report;
        }

        // Resultレポートのヘッダ
        public virtual string ResultReportHeader()
        {
            return string.Format("{0}", "ProfitMargin");
        }

        // Resultレポートの出力値
        public virtual string ResultReportValues()
        {
            return string.Format("{0}", ProfitMargin());
        }
    }
}
