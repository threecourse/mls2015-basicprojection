using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BasicProjection
{
    // 終身保険モデルの基本クラス
    class WLModel : BaseModel
    {
        public WLModel(Assumption asm, Cell cell): base(asm, cell){}

        // レート
        protected override BaseRate Set_Rate()
        {
            return new WLRate(asm, x, n, m, sex);
        }

        // 死亡率・解約率・選択効果などの配列
        protected override double[] Set_qx_input()
        {
            if (sex == "M")
            { return asm.get_dblary("qx_m_SLT1996"); }
            else
            { return asm.get_dblary("qx_f_SLT1996"); }
        }

        protected override double[] Set_qwx_input()
        {
            return asm.get_dblary("LapseRate");
        }

        protected override double[] Set_SelectionFactor_input()
        {
            return asm.get_dblary("SelectionFactor");
        }

        // 事業費・コミッション・運用利回りの前提
        protected override double Set_Expense_AcqS()
        {
            return asm.get_dbl("Expense_AcqS");
        }

        protected override double Set_Expense_AcqP()
        {
            return asm.get_dbl("Expense_AcqP");
        }

        protected override double Set_Expense_MaintpayingS()
        {
            return asm.get_dbl("Expense_MaintpayingS");
        }

        protected override double Set_Expense_MaintpaidupS()
        {
            return asm.get_dbl("Expense_MaintpaidupS");
        }

        protected override double Set_Expense_MaintP()
        {
            return asm.get_dbl("Expense_MaintP");
        }
        
        // 保険金額・コミッション率
        public override double DeathBen(int t)
        {
            if (t == 0 || t > n) { return 0.0; }
            return 1.0;
        }

        public override double SurrenderBen(int t)
        {
            if (t == 0 || t > n) { return 0.0; }
            return Rate.CV_Rate[t];
        }

        public override double PartialEndowBen(int t)
        {
            return 0.0;
        }

        public override double EndowBen(int t)
        {
            return 0.0;
        }

        public override double CommRatio(int t)
        {
            return 0.0;
        }
    }
}