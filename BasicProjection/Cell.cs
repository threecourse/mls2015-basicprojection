﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasicProjection
{
    // セル
    public class Cell
    {
        Dictionary<string, string> dic;

        // 型を指定して値を取得する
        public double get_dbl(string name) { return double.Parse(dic[name]); }
        public int get_int(string name) { return int.Parse(dic[name]); }
        public bool get_bool(string name) { return bool.Parse(dic[name]); }
        public string get_str(string name) { return dic[name]; }

        // ファイルからCellのListを作成する
        static public List<Cell> LoadCells(string filename)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(filename);
            List<string> items = new List<string>();
            List<Cell> ret = new List<Cell>();

            for (int r = 0; sr.Peek() >= 0; r++)
            {
                string[] line = sr.ReadLine().Split(',');

                if (r == 0)
                {
                    line.Foreach(e => items.Add(e));
                }
                else
                {
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    for (int i = 0; i < items.Count; i++) { dic.Add(items[i], line[i]); }
                    ret.Add(CreateCell(dic));
                }
            }
            return ret;
        }

        static Cell CreateCell(Dictionary<string, string> dic)
        {
            Cell c = new Cell();
            c.dic = dic;
            return c;
        }

       
    }

}
