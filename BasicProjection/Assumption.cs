﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasicProjection
{
    // アサンプション
    public class Assumption
    {
        Dictionary<string, List<string>> dic;

        // 型を指定して値を取得する
        public double[] get_dblary(string name) { return dic[name].Select(v => double.Parse(v)).ToArray(); }
        public int[] get_intary(string name) { return dic[name].Select(v => int.Parse(v)).ToArray(); }
        public bool[] get_boolary(string name) { return dic[name].Select(v => bool.Parse(v)).ToArray(); }
        public string[] get_strary(string name) { return dic[name].ToArray(); }

        public double get_dbl(string name) { return double.Parse(dic[name][0]); }
        public int get_int(string name) { return int.Parse(dic[name][0]); }
        public bool get_bool(string name) { return bool.Parse(dic[name][0]); }
        public string get_str(string name) { return dic[name][0]; }

        // ファイル名から読み込みAssumptionを作成する
        public static Assumption Load(string filename)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(filename);
            Dictionary<string, List<string>> dic = new Dictionary<string, List<string>>();

            for (int r = 0; sr.Peek() >= 0; r++)
            {
                int KeyIndex = 0;
                string[] line = sr.ReadLine().Split(',');

                string key = line[KeyIndex];
                List<string> values = line.Skip(KeyIndex + 1).ToList();

                dic.Add(key,values);
            }
            Assumption ret = new Assumption();
            ret.dic = dic;
            return ret;
        }
    }
}
