using System;
using System.Collections.Generic;

namespace BasicProjection
{
    // 基数クラス
    class CommutationTable
    {
        // input
        public double CIR;
        public double[] qx_input;

        // 生命表・基数
        public double v;
        public double[] qx = new double[Setting.MaxX + 1];
        public double[] lx = new double[Setting.MaxX + 1];
        public double[] d_x = new double[Setting.MaxX + 1];
        public double[] Dx = new double[Setting.MaxX + 1];
        public double[] Cx = new double[Setting.MaxX + 1];
        public double[] Nx = new double[Setting.MaxX + 1];
        public double[] Mx = new double[Setting.MaxX + 1];

        // コンストラクタ
        public CommutationTable(double CIR, double[] _qx)
        {
            this.CIR = CIR;
            this.qx_input = _qx;

            Initialize();
            Calculate();
        }

        // 配列等の初期化
        protected void Initialize()
        {
            qx = new double[Setting.MaxX + 1];
            lx = new double[Setting.MaxX + 1];
            d_x = new double[Setting.MaxX + 1];
            Dx = new double[Setting.MaxX + 1];
            Cx = new double[Setting.MaxX + 1];
            Nx = new double[Setting.MaxX + 1];
            Mx = new double[Setting.MaxX + 1];
        }

        // 計算
        protected void Calculate()
        {

            v = 1.0 / (1.0 + CIR);
            for (int x = 0; x <= Setting.MaxX; x++)
            {
                qx[x] = x < qx_input.Length ? qx_input[x] : 1.0;
            }

            for (int x = 0; x <= Setting.MaxX; x++)
            {
                lx[x] = (x == 0) ? 1.0 : (lx[x - 1] - d_x[x - 1]);
                d_x[x] = lx[x] * qx[x];
                Dx[x] = lx[x] * Math.Pow(v, x);
                Cx[x] = d_x[x] * Math.Pow(v, x + 0.5);
            }

            for (int x = Setting.MaxX; x >= 0; x--)
            {
                Nx[x] = (x == Setting.MaxX) ? Dx[x] : (Dx[x] + Nx[x + 1]);
                Mx[x] = (x == Setting.MaxX) ? Cx[x] : (Cx[x] + Mx[x + 1]);
            }
        }

        // 年金現価
        public double Annuity_x(int x)
        {
            if (Dx[x] <= 0) { return 0.0; }
            return Nx[x] / Dx[x];
        }

        public double Annuity_x_k(int x, int n, int k)
        {
            if (Dx[x] <= 0 || n < 0) { return 0.0; }
            return Annuity_x(x) - (k - 1.0) / (2.0 * k);
        }

        public double Annuity_xn(int x, int n)
        {
            if (Dx[x] <= 0 || n < 0) { return 0.0; }
            return (Nx[x] - Nx[x + n]) / Dx[x];
        }

        public double Annuity_xn_k(int x, int n, int k)
        {
            if (Dx[x] <= 0 || n < 0) { return 0.0; }
            return Annuity_xn(x, n) - (k - 1.0) / (2.0 * k) * (1.0 - Dx[x + n] / Dx[x]);
        }

        // レポート
        public List<string> DumpReport()
        {
            List<string> report = new List<string>();
            report.AddRecord("Dx", Dx);
            report.AddRecord("Cx", Cx);
            report.AddRecord("Nx", Nx);
            report.AddRecord("Mx", Mx);
            return report;
        }

    }
}