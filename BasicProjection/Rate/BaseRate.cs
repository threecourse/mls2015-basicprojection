using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BasicProjection
{
    // レートの基本クラス
    abstract class BaseRate
    {
        // インプット
        public Assumption asm;
        public int x;
        public int n;
        public int m;
        public string sex;

        // 死亡率・基数
        public double[][] _qx;
        public CommutationTable[] CT = new CommutationTable[2];

        // レート
        public double BaseP_Rate;
        public double GrossP_Rate;
        public double[] CV_Rate;
        public double[] NetP_Rate;
        public double[][] NetV_Rate;
        public double[] AllZillV_Rate;

        // コンストラクタ
        public BaseRate(Assumption asm, int x, int n, int m, string sex)
        {
            this.asm = asm;
            this.x = x;
            this.n = n;
            this.m = m;
            this.sex = sex;

            Initialize();
            Calculate();
        }

        // 配列等の初期化
        protected virtual void Initialize(){

          _qx = new double[2][];
          CT = new CommutationTable[2];
          
          CV_Rate = new double[Setting.MaxT + 1];
          NetP_Rate = new double[2];
          NetV_Rate = new double[2][];
          NetV_Rate[0] = new double[Setting.MaxT + 1];
          NetV_Rate[1] = new double[Setting.MaxT + 1];
          AllZillV_Rate = new double[Setting.MaxT + 1];

        }

        // 計算
        protected virtual void Calculate()
        {
            _qx[0] = F_qx(0);
            _qx[1] = F_qx(1);
            CT[0] = F_CT(0);
            CT[1] = F_CT(1);

            BaseP_Rate = F_BaseP_Rate();
            GrossP_Rate = F_GrossP_Rate();
            NetP_Rate[0] = F_NetP_Rate(0);
            NetP_Rate[1] = F_NetP_Rate(1);

            for (int t = 0; t < Setting.MaxT; t++)
            {
                NetV_Rate[0][t] = F_NetV_Rate(t, 0);
                NetV_Rate[1][t] = F_NetV_Rate(t, 1);
                CV_Rate[t] = F_CV_Rate(t);
                AllZillV_Rate[t] = F_AllZillV_Rate(t);
            }
        }
        
        // 計算ロジックの定義
        // F_で始まる関数で計算ロジックを定義する
        // 未計算の変数を使うと計算結果がおかしくなるので注意が必要
        protected double[] F_qx(int kiso)
        {
            if (sex == "M") { return asm.get_dblary("qx_m_SLT1996"); }
            else { return asm.get_dblary("qx_f_SLT1996"); }
        }

        protected virtual CommutationTable F_CT(int kiso)
        {
            double CIR;
            if (kiso == 0) { CIR = asm.get_dbl("CIR_P"); }
            else { CIR = asm.get_dbl("CIR_V"); }
            return new CommutationTable(CIR, _qx[kiso]);
        }

        protected virtual double F_GrossP_Rate()
        {
            return BaseP_Rate * 12;
        }

        protected abstract double F_BaseP_Rate();
        protected abstract double F_CV_Rate(int t);
        protected abstract double F_NetP_Rate(int kiso);
        protected abstract double F_NetV_Rate(int t, int kiso);
        protected abstract double F_AllZillV_Rate(int t);

        // レポート
        public List<string> DumpReport()
        {
            List<string> report = new List<string>();
            report.AddRange(CT[0].DumpReport());
            report.AddRange(CT[1].DumpReport());
            report.AddRecordSingle("GrossP_Rate", GrossP_Rate);
            report.AddRecordSingle("BaseP_Rate", BaseP_Rate);
            report.AddRecordSingle("NetP_Rate_P", NetP_Rate[0]);
            report.AddRecordSingle("NetP_Rate_V", NetP_Rate[1]);
            report.AddRecord("CV_Rate", CV_Rate);
            report.AddRecord("NetV_Rate_P", t => NetV_Rate[0][t]);
            report.AddRecord("NetV_Rate_V", t => NetV_Rate[1][t]);
            report.AddRecord("ZillV_Rate", AllZillV_Rate);
            return report;
        }
    }
}