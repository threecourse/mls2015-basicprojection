using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BasicProjection
{
    // 終身保険レートのクラス
    class WLRate : BaseRate
    {
        // コンストラクタ
        public WLRate(Assumption asm, int x, int n, int m, string sex) : base(asm, x, n, m, sex) { }

        // 予定事業費
        public virtual double Gamma() { return 0.0; }
        public virtual double Alpha() { return 0.015; }
        public virtual double Beta() { return 0.1; }
        public virtual double ZillAlpha() { return Alpha(); }

        // レート関数の定義
        protected override double F_BaseP_Rate()
        {
            // monthly rate
            int kiso = 0;
            double numer = (CT[kiso].Mx[x] - CT[kiso].Mx[x + n]) / CT[kiso].Dx[x]
                         + Alpha()
                         + CT[kiso].Annuity_xn(x, n) * Gamma();
            double denom = (1 - Beta()) * CT[kiso].Annuity_xn_k(x, m, 12) * 12;
            return numer / denom;
        }

        protected override double F_CV_Rate(int t)
        {
            int kiso = 0;
            if (t < 10) { return Math.Max(NetV_Rate[kiso][t] - ZillAlpha() * (10.0 - t) / 10.0, 0.0); }
            return NetV_Rate[kiso][t];
        }

        protected override double F_NetP_Rate(int kiso)
        {
            double numer = (CT[kiso].Mx[x] - CT[kiso].Mx[x + n]) / CT[kiso].Dx[x]
                           + CT[kiso].Annuity_xn(x, n) * Gamma();
            double denom = CT[kiso].Annuity_xn(x, m);
            return numer / denom;
        }

        protected override double F_NetV_Rate(int t, int kiso)
        {
            if (t > n) { return 0.0; }
            if (CT[kiso].Dx[x + t] <= 0.0) { return 0.0; }
            return (CT[kiso].Mx[x + t] - CT[kiso].Mx[x + n]) / CT[kiso].Dx[x + t]
                  + CT[kiso].Annuity_xn(x + t, n - t) * Gamma()
                  - CT[kiso].Annuity_xn(x + t, m - t) * NetP_Rate[kiso];
        }

        protected override double F_AllZillV_Rate(int t)
        {
            int zm = Math.Min(m, Setting.MaxT);
            int kiso = 1; // Vkiso
            if (t < zm)
            {
                return NetV_Rate[kiso][t]
                     - ZillAlpha() * CT[kiso].Annuity_xn(x + t, zm - t) / CT[kiso].Annuity_xn(x, zm);
            }
            return NetV_Rate[kiso][t];
        }
    }
}